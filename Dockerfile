FROM node:16-alphine

WORKDIR /app/colormass

COPY package*.json ./

USER node

RUN npm i

COPY . .

EXPOSE 4000

CMD ["node", "server.js"]