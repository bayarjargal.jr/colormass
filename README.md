# colormass

This is a repository for practical assignment. 

## Tech stack  

- ExpressJS (NodeJS)
- PostgreSQL for database
- REST API and GraphQL
- AWS EC2 for staging VM
- AWS S3 bucket for file management
- AWS RDS for staging database (PostgreSQL)

## Database architecture

![ERD generated form PGAdmin](./erd.png)
> Some of lines are missing, PGAdmin didn't export the diagram properly.


## Thought process

First of all I've tried to use as many technologies and languages as I have experience. Such as Javascript for development,
REST API and Graphql for backend architecture, AWS EC2 for staging server, AWS S3 bucket for file management and 
AWS RDS for staging database. 

#### Designing architecture

I have used simple JWT  (JSON Web Token) for REST API.

First, I tought that I could create entity (Database table) for image file , text file and folder. However I've changed my mind and created single entity that has file type. So I can self join ```File``` table and set delete mode to ***CASCADE***. This simplifies permission checks. 


## Problems and Solutions

#### Problem 1: Uploading and downloading files 

One of the problems was uploading huge files. Saving huge files to database is obviously a bad idea. 
Uploading files to server disk and saving files path to database was a optimal solution for me. 
But uploading single large file would increase memory consumption significantly. 

#### Solution 1.1: Uploading files through stream

Creating file stream directly to server disk while we uploading data was one of the solution.
We can limit our stream buffer size to optimal size. It would decrease memory consumption drastically.

```javascript

const fs = require("fs");
const path = require("path");

// This is called from router
const controller = (req, res, next) => {
    try {
        // Create a write stream
        var stream = fs.createWriteStream(path.join(__dirname, pathToFile), { flags: "a" });

        // Pipe the stream
        req.pipe(stream);

        // If the stream throws and error return HTTP status 500
        stream.on('error', err => {
            res.status(500).send({ message:"Something went wrong" });
        })
        
        // If the stream succesfully closes return success message 
        stream.on('close', err => {
            res.send({ message:"Succesfully uploaded files" });
        })

    } catch(error) {

        // Return error
        res.status(500).send({ message:"Something went wrong" });

    }
}

```

#### Solution 1.2: Uploading files using AWS S3 multipart download

It is challenging to upload files using GraphQL mutation. 
There are few solutions that has multipart upload and buffers. For example, package called ```graphql-upload```. This package
uses other packages like ```graphql-multipart-request-spec```, ```busboy```.

But for me, it is easier to upload file to AWS S3 bucket and save object keys to database.
By using AWS S3 bucket I am no longer worrying about handling files.
However, downside of using AWS S3 is money :).

#### Problem 2: Permission checks 

It was easier to implement in ExpressJS, because we can use simple middleware that check
file or folder's permission before executing actions.

#### Solution: Permission checks /REST API/ 

I have created permission that is identical to Linux file permissions, but simpler.
It consists 3 integers like ```000, 010```.

- If a user or a group has read permission on specific file: `1**`
- If a user or a group has modification permission(create,update) on specific file: `*1*`
- If a user or a group has permission to delete specific file: `**1`

Therefore we can easilty check permission.

### Code
Write custom middleware for file routes that can handle permissions. 
- Get user id from JWT
- Get file permissions using file key and user id
- Return the file or modify file if permission is granted, otherwise throw an error

```javascript

module.export = function(req,res,next) {

    const userId = getUserIdFromJWT(req);
    const fileId = req.params.fileId;

    // I am using sequelize ORM, it supports regex
    let regex = "";
    if (action === "read") {
        regex = "^1"
    } else if (action === "update") {
        regex = "^.{1}1.{1}$";
    } else {
        regex = "1$";
    }

    const perm = await Permission.findOne({
        where: {
            userId,
            fileId,
            value: {
                [Op.regexp]: regex
            }
        }
    });

    if(!permission) return res.status(401).send({ message: "Access denied" });

    const file = FileModel.findByPk(fileId);

    // Conitune processing request
    next()

}

```
