const express = require("express");
const db = require("./src/models");
const bodyParser = require("body-parser");

const { graphqlHTTP } = require('express-graphql');
const { buildSchema } = require('graphql');
require("dotenv").config();

const app = express();
app.use(bodyParser.json());
require('./src/utils/passport');

db.sequelize.authenticate().then(() => console.log("Successfully connected to DB"));

var schema = buildSchema(`
  type Query {
    createFile: String
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  createFile: () => {
    return 'Hello world!';
  },
};
app.use(require("./src/routes"));

app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));

app.listen(4000, () => {
  db.sequelize.sync({ force: true, alter: false })
});
