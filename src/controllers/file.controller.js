const { getUserIDFromJWT } = require("../utils/jwt");
const { fileList, createFile, folderFiles, deleteFileOrFolder } = require("../functions/file.functions");

exports.upload = async (req, res, next) => {
    try {
        const createdBy = getUserIDFromJWT(req);
        const { mimetype, key, encoding, } = req.files[0];

        const payload = {
            mimetype,
            encoding,
            parent: req.body.parent,
            createdBy,
            name: key
        }

        const data = await createFile(payload);

        return res.send(data);
    } catch (error) {
        next(error)
    }
}

exports.list = async (req, res, next) => {
    try {
        const size = parseInt(req.query.size || 10, 10);
        const page = parseInt(req.query.page || 0, 10);

        const { rows, count } = fileList(req.body, size, page);

        return res.send({
            list: rows ? rows.map(e => ({ ...e.dataValues, hash: undefined, salt: undefined })) : [],
            totalElements: count,
            totalPages: parseInt(count / size)
        });

    } catch (error) {
        next(error);
    }
}

exports.update = async (req, res, next) => {
    try {
        const fileId = req.params.id;
        res.send({ message: fileId })
    } catch (error) {
        next(error);
    }
}


exports.download = async (req, res, next) => {
    try {
        const fileId = req.params.id;
        res.send({ message: fileId })
    } catch (error) {
        next(error);
    }
}

exports.delete = async (req, res, next) => {
    try {
        const data = await deleteFileOrFolder(req.params.id);
        res.send(data)
    } catch (error) {
        next(error);
    }
}


exports.deleteFolder = async (req, res, next) => {
    try {
        const data = await deleteFileOrFolder(req.params.id);
        return res.send(data);
    } catch (error) {
        next(error);
    }
}

exports.updateFolder = async (req, res, next) => {
    try {
        const fileId = req.params.id;
        res.send({ message: fileId })
    } catch (error) {
        next(error);
    }
}


exports.listFolder = async (req, res, next) => {
    try {
        const fileId = req.params.id;

        res.send({ message: fileId })
    } catch (error) {
        next(error);
    }
}

exports.createFolder = async (req, res, next) => {
    try {
        const createdBy = getUserIDFromJWT(req);
        const payload = {
            mimetype: "folder",
            type: "folder",
            encoding: "UTF8",
            name: req.body.name,
            parent: req.body.parent,
            createdBy
        }

        const data = await createFile(payload);
        return res.send(data)
    } catch (error) {
        next(error);
    }
}

exports.folderDetails = async (req, res, next) => {
    try {
        const files = await folderFiles(req.params.id)
        res.send(files)
    } catch (error) {
        next(error);
    }
}