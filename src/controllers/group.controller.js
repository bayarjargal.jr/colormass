const { getUserIDFromJWT } = require("../utils/jwt");
const { deleteGroup, groupList, updateGroup, createGroup, groupDetails, addUser } = require("../functions/group.functions");

exports.delete = async (req, res, next) => {
    try {
        const ret = await deleteGroup(req.params.id);
        return res.send(ret);
    } catch (error) {
        return next(error);
    }
};


exports.list = async (req, res, next) => {

    try {
        const size = parseInt(req.query.size || 10, 10);
        const page = parseInt(req.query.page || 0, 10);

        const { rows, count } = await groupList(req.body, size, page);

        return res.send({
            list: rows,
            totalElements: count,
            totalPages: parseInt(count / size)
        });

    } catch (error) {
        return next(error);
    }

};

exports.update = async (req, res, next) => {
    try {
        const data = await updateGroup(req.params.id, { ...req.body, id: undefined });
        return res.send(data.dataValues);

    } catch (error) {
        return next(error);
    }
}
exports.create = async (req, res, next) => {
    try {
        const createdBy = getUserIDFromJWT(req);
        const data = await createGroup({ ...req.body, createdBy });
        return res.send(data.dataValues);

    } catch (error) {
        return next(error);
    }
}

exports.details = async (req, res, next) => {
    try {
        const data = await groupDetails(req.params.id);
        return res.send(data);
    } catch (error) {
        return next(error);
    }
}

exports.addUser = async (req, res, next) => {
    try {
        const data = await addUser(req.body);
        return res.send(data);
    } catch (error) {
        return next(error);
    }
}
