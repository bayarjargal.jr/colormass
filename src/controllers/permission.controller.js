const { grantPermission } = require("../functions/permission.functions");

exports.grant = async (req, res, next) => {
    try {
        const ret = await grantPermission(req.body);
        return res.send(ret);
    } catch (error) {
        return next(error);
    }
};

