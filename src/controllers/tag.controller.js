const { getUserIDFromJWT } = require("../utils/jwt");
const db = require("../models");
const { deleteTag, tagList, updateTag, createTag, tagDetails, addTag } = require("../functions/tag.functions");

exports.delete = async (req, res, next) => {
    try {
        const ret = await deleteTag(req.params.id);
        return res.send(ret);
    } catch (error) {
        return next(error);
    }
};

exports.addTag = async (req, res, next) => {
    try {
        const ret = await addTag(req.body);
        return res.send(ret);
    } catch (error) {
        return next(error);
    }
};


exports.list = async (req, res, next) => {

    try {
        const size = parseInt(req.query.size || 10, 10);
        const page = parseInt(req.query.page || 0, 10);

        const { rows, count } = await tagList(req.body, size, page);

        return res.send({
            list: rows,
            totalElements: count,
            totalPages: parseInt(count / size)
        });

    } catch (error) {
        return next(error);
    }

};

exports.update = async (req, res, next) => {
    try {
        const data = await updateTag(req.params.id, { ...req.body, id: undefined });
        return res.send(data.dataValues);

    } catch (error) {
        return next(error);
    }
}
exports.create = async (req, res, next) => {
    try {
        const createdBy = getUserIDFromJWT(req);
        const data = await createTag({ ...req.body, createdBy });
        return res.send(data.dataValues);

    } catch (error) {
        return next(error);
    }
}

exports.details = async (req, res, next) => {
    try {
        const data = await tagDetails(req.params.id);
        return res.send(data.dataValues);
    } catch (error) {
        return next(error);
    }
}
