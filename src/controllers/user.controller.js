const { generateJWT } = require('../utils/jwt');
const passport = require("passport");
const { createUser, userList, deleteUser, updateUser } = require("../functions/user.functions");

exports.delete = async (req, res, next) => {
    try {
        const data = await deleteUser(req.params.id);
        return res.send(data);
    } catch (error) {
        return next(error);
    }
};


exports.list = async (req, res, next) => {

    try {
        const size = parseInt(req.query.size || 10, 10);
        const page = parseInt(req.query.page || 0, 10);

        const { rows, count } = await userList(req.body, size, page);

        return res.send({
            list: rows ? rows.map(e => ({ ...e.dataValues, hash: undefined, salt: undefined })) : [],
            totalElements: count,
            totalPages: parseInt(count / size)
        });

    } catch (error) {
        return next(error);
    }

};

exports.update = async (req, res, next) => {
    try {
        const data = await updateUser(req.params.id, { ...req.body, id: undefined, hash: undefined, salt: undefined });
        return res.send({ ...data, hash: undefined, salt: undefined });

    } catch (error) {
        return next(error);
    }
}
exports.create = async (req, res, next) => {
    try {
        const data = await createUser(req.body);
        return res.send({ ...data.dataValues, hash: undefined, salt: undefined });

    } catch (error) {
        return next(error);
    }
}

exports.login = async (req, res, next) => {
    passport.authenticate('local', { session: false }, async (err, user, info) => {
        console.log(info)
        if (err) { return next(err); }
        if (!user) return next(info);
        return res.send({ ...user.dataValues, token: generateJWT(user.id), hash: undefined, salt: undefined });
    })(req, res, next);
}