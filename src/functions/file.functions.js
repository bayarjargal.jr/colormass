const fs = require("fs");
const path = require("path");
const db = require("../models");
const { filter } = require('../utils/filter');
const AWS = require('../utils/aws');

const Tag = db.tag;
const FilesTag = db.filesTag;
const FileModel = db.file;
const Permission = db.permission;

exports.createFile = async function (payload) {

    try {
        if (Boolean(payload.parent)) {
            const parent = await FileModel.findOne({ where: { id: payload.parent, type: "folder" } });
            if (!parent) throw new Error("Parent folder is not found");
        }
        const data = await FileModel.create(payload);

        await Permission.create({
            fileId: data.dataValues.id,
            userId: payload.createdBy,
            // Current user is file(folder) owner, so current user has full permission  
            value: "111"
        });

        return { message: "Success", ...data.dataValues };
    } catch (error) {
        throw new Error(error);
    }
}

exports.fileList = async function (payload, size, page) {
    try {
        const size = parseInt(size || 10, 10);
        const page = parseInt(page || 0, 10);

        const data = await filter(FileModel, payload, { size, page });
        return data;
    } catch (error) {

        throw new Error(error);
    }
}

exports.createFolder = async function (payload) {
    try {
        if (Boolean(payload.parent)) {
            const parent = await FileModel.findOne({ where: { id: payload.parent, type: "folder" } });
            if (!parent) throw new Error("Parent folder is not found");
        }
        const values = {
            ...payload,
            mimetype: "folder",
            encoding: "UTF-8"
        }
        const data = await FileModel.create(values);
    } catch (error) {

    }
}


exports.updateFolder = async function (id, payload) {
    try {
        const folder = await FileModel.findOne({ where: { id, type: "folder" } });
        await folder.update({ name: payload.name });
        return folder;
    } catch (error) {
        console.log(error)
        throw new Error(error.errors || error);
    }
}

exports.deleteFileOrFolder = async function (id) {
    try {
        //
        // It deletes all files and folders within folder if specific item is folder. Because we've used CASCADE mode on file model. 
        //
        const file = await FileModel.findByPk(id);
        await file.destroy();

        if (file.dataValues.type === "file") {

            // Delete from AWS or file system
            // If AWS is configured delete from S3 bucket otherwise delete from disk

            if (process.env.AWS_ACL_KEY && process.env.AWS_ACL_ID) {
                const s3 = new AWS.S3();
                const params = {
                    Bucket: "colormass",
                    Key: file.name
                }
                try {
                    await s3.headObject(params).promise()
                    try {
                        await s3.deleteObject(params).promise()
                    } catch (error) {

                        throw new Error("Couln't delete file")
                    }
                } catch (error) {
                    throw new Error("File not found")
                }
            } else {
                await fs.unlink(path.join(__dirname, "..", "..", "public", "uploads", file.name));
            }
        }
        return { message: "Successfully deleted folder and files" };
    } catch (error) {
        throw new Error(error.errors || error);
    }
}

exports.folderFiles = async function (id) {
    try {
        const files = await FileModel.findAll({
            where: { parent: id }, include: [{
                model: FileModel,
                as: "child",
                required: false
            }]
        });
        return files.map(e => e.dataValues);

    } catch (error) {
        console.log(error)
        throw new Error(error.errors || error);
    }
}