const db = require("../models");
const { filter } = require("../utils/filter");
const groupValidation = require("../validation/group.validation");

const Group = db.group;
const User = db.user;
const UserGroup = db.userGroup;

exports.updateGroup = async function (id, payload) {
    try {
        const group = await Group.findByPk(id);
        await group.update(payload);
        return group;
    } catch (error) {
        console.log(error)
        throw new Error(error.errors || error);
    }
}

exports.createGroup = async function (payload) {
    try {
        const { createdBy } = payload;
        await groupValidation.validate(payload, { abortEarly: false });
        const data = await Group.create(payload);

        let included = false;

        if (payload.users && payload.users.length > 0) {
            for (const user of payload.users) {
                await UserGroup.create({
                    userId: user,
                    groupId: data.dataValues.id
                })
            }
            if (payload.users.findIndex(e => e === createdBy) < 0) included = true;
        }

        if (included)
            await UserGroup.create({
                userId: createdBy,
                groupId: data.dataValues.id
            })


        return data;
    } catch (error) {
        throw new Error(error.errors || error);
    }
}

exports.groupList = async function (payload, s, p) {
    try {
        const size = parseInt(s || 10, 10);
        const page = parseInt(p || 0, 10);

        const data = await filter(Group, payload, { size, page });
        return data;
    } catch (error) {

        throw new Error(error);
    }
}

exports.deleteGroup = async function (id) {
    try {
        await Group.destroy({ where: { id } });
        return { message: "Successfully deleted group" };
    } catch (error) {
        throw new Error(error);
    }
}

exports.groupDetails = async function (id) {
    try {
        const group = await Group.findByPk(id);

        const members = await User.findAll({
            include: {
                model: Group,
                where: {
                    id
                },
                through: {
                    attributes: []
                }
            }
        });

        return { ...group.dataValues, members: members.map((e) => ({ ...e.dataValues, hash: undefined, salt: undefined })) };
    } catch (error) {
        console.log(error)
        throw new Error(error.errors || error);
    }
}

exports.addUser = async function (payload) {
    try {
        await UserGroup.create(payload)
        return { message: "Successfully added an user" };
    } catch (error) {
        console.log(error)
        throw new Error(error.errors || error);
    }
}