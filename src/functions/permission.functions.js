const db = require("../models");

const Permission = db.permission;

/**
 * @description
 * If permission exists update otherwise create a new entry
 */

exports.grantPermission = async function (payload) {
    try {
        const perm = await Permission.findOne({
            where: {
                userId: payload.userId,
                fileId: payload.fileId,
            }
        })
        if (!perm)
            await Permission.create(payload);
        else perm.update({ value: payload.value })

        return { message: "Permission granted" };
    } catch (error) {
        console.log(error)
        throw new Error(error.errors || error);
    }
}