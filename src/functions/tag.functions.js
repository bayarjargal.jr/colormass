const db = require("../models");
const { filter } = require("../utils/filter");
const tagValidation = require("../validation/tag.validation");

const Tag = db.tag;
const FilesTag = db.filesTag;

/**
 * @description
 * Adds a tag to specific file
 */

exports.addTag = async function (payload) {
    try {
        await FilesTag.create(payload);
        return { message: "Successfully added a tag" };
    } catch (error) {
        console.log(error)
        throw new Error(error.errors || error);
    }
}

/**
 * @description
 * Updates an entry on database
 */

exports.updateTag = async function (id, payload) {
    try {
        const tag = await Tag.findByPk(id);
        await tag.update(payload);
        return tag;
    } catch (error) {
        console.log(error)
        throw new Error(error.errors || error);
    }
}

/**
 * @description
 * Creates a new entry on database
 */
exports.createTag = async function (payload) {
    try {
        await tagValidation.validate(payload, { abortEarly: false });
        const data = await Tag.create(payload);
        return data;
    } catch (error) {
        console.log(error)
        throw new Error(error.errors || error);
    }
}

/**
 * @description
 * Returns tags list with pagingation
 */

exports.tagList = async function (payload, s, p) {
    try {
        const size = parseInt(s || 10, 10);
        const page = parseInt(p || 0, 10);

        const data = await filter(Tag, payload, { size, page });
        console.log(data)
        return data;
    } catch (error) {

        throw new Error(error);
    }
}

/**
 * @description
 * Deletes a tag from the database
 */

exports.deleteTag = async function (id) {
    try {
        await Tag.destroy({ where: { id } });
        return { message: "Successfully deleted tag" };
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * @description
 * Returns specific tag details
 */

exports.tagDetails = async function (id) {
    try {
        const tag = await Tag.findByPk(id);
        return tag;
    } catch (error) {
        console.log(error)
        throw new Error(error.errors || error);
    }
}