const crypto = require('crypto');
const passport = require("passport");

const { filter } = require("../utils/filter");
const { getUserIDFromJWT, generateJWT } = require('../utils/jwt');

const db = require("../models");
const userValidation = require("../validation/user.validation");

const User = db.user;
exports.updateUser = async function (id, payload) {
    try {
        const user = await User.findByPk(id);
        await user.update(payload);
        return { ...user.dataValues, hash: undefined, salt: undefined };
    } catch (error) {
        console.log(error)
        throw new Error(error.errors || error);
    }
}

exports.createUser = async function (payload) {
    try {
        await userValidation.validate(payload, { abortEarly: false })
        const salt = crypto.randomBytes(16).toString('hex');
        const hash = crypto.pbkdf2Sync(payload.password, salt, 10000, 512, 'sha512').toString('hex');
        let user = {
            firstname: payload.firstname,
            lastname: payload.lastname,
            email: payload.email,
            hash,
            salt
        };
        const data = await User.create(user);
        return data;
    } catch (error) {
        console.log(error)
        throw new Error(error.errors || error);
    }
}

exports.userList = async function (payload, s, p) {
    try {
        const size = parseInt(s || 10, 10);
        const page = parseInt(p || 0, 10);

        const data = await filter(User, payload, { size, page });
        return data;
    } catch (error) {

        throw new Error(error);
    }
}

exports.deleteUser = async function (id) {
    try {
        await User.destroy({ where: { id } });
        return { message: "Successfully deleted user" };
    } catch (error) {

        throw new Error(error);
    }
}
