module.exports = function (error, req, res, next) {

    console.log(error);

    if (error.name === 'Error')
        return res.status(400).send({ message: "Error occured", errors: [error.message] });

    if (error.name === 'UnauthorizedError') {
        return res.status(403).send({ message: "Please login" });
    }
    if (error.name.includes("Sequelize")) {
        if (error.name === "SequelizeForeignKeyConstraintError") {
            if (error.fields.indexOf("userId") >= 0) {
                return res.status(400).send({ message: "Error occured", errors: ["User not found"] })
            }
        }

        if (error.errors)
            return res.status(400).send({ message: "Алдаа гарлаа", errors: error.errors.map(e => e.message) })
    }
    return res.status(500).send({
        message: "Error occured",
        errors: error.errors || []
    })
}
