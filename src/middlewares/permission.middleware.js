const { Op } = require("sequelize");
const db = require("../models");
const Permission = db.permission;
const FileModel = db.file;
const { getUserIDFromJWT } = require("../utils/jwt");

exports.check = function (action) {
    return async function (req, res, next) {
        try {
            const userId = getUserIDFromJWT(req);
            const fileId = req.params.id;

            let regex = "";
            if (action === "read") {
                regex = "^1"
            } else if (action === "update") {
                regex = "^.{1}1.{1}$";
            } else {
                regex = "1$";
            }

            const perm = await Permission.findOne({
                where: {
                    userId,
                    fileId,
                    value: {
                        [Op.regexp]: regex
                    }
                }
            });
            if (!perm) {
                return res.status(401).send({ message: "Access denied" });
            };
            let f = perm;

            // Traverse through parent directories
            // while (!Boolean(f)) {
            //     const parentPermission = await Permission.findOne({
            //         include: {
            //             model: FileModel,
            //             where: {
            //                 id: f.parent
            //             }
            //         }
            //     });

            //     f 
            // }
            next();
        } catch (error) {
            console.log(error)
            res.status(500).send({ message: "Internal server error" })
        }
    }
}