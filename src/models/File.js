
module.exports = (sequelize, DataTypes) => {
    const FileModel = sequelize.define("file", {
        type: {
            type: DataTypes.STRING,
            defaultValue: "file",
            validate: {
                customValidator: (value) => {
                    const enums = ["file", "folder"]
                    if (!enums.includes(value)) {
                        throw new Error('Not a valid option')
                    }
                }
            }
        },
        mimetype: {
            type: DataTypes.STRING,
        },
        encoding: {
            type: DataTypes.STRING,
        },
        name: {
            type: DataTypes.STRING,
        },
        parent: {
            type: DataTypes.INTEGER,
        },
        createdBy: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    });
    return FileModel;
};