
module.exports = (sequelize, DataTypes) => {
    const Group = sequelize.define("group", {
        name: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        createdBy: {
            type: DataTypes.INTEGER,
            allowNull: false
        },


    });
    return Group;
};