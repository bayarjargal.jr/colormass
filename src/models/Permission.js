
module.exports = (sequelize, DataTypes) => {
    const Permission = sequelize.define("permission", {
        value: {
            type: DataTypes.STRING,
            defaultValue: "111",
            validate: {
                customValidator: (value) => {
                    const enums = ["000", "001", "010", "011", "100", "101", "110", "111"]
                    if (!enums.includes(value)) {
                        throw new Error('Not a valid option')
                    }
                }
            }
        },

        //Relation fields are auto generated
    });
    return Permission;
};