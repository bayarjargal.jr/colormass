
module.exports = (sequelize, DataTypes) => {
    const Tag = sequelize.define("tag", {
        name: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        createdBy: {
            type: DataTypes.INTEGER,
            allowNull: false
        },


    });
    return Tag;
};