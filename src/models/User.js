const moment = require("moment");
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define("user", {
        firstname: {
            type: DataTypes.STRING(50),
            allowNull: false,
        },
        lastname: {
            type: DataTypes.STRING(50),
            allowNull: false
        },

        email: {
            type: DataTypes.STRING(100),
            allowNull: true,
            unique: {
                args: true,
                msg: "E-Mail is already registered"
            },
            validate: {
                isEmail: {
                    args: true,
                    msg: "Invalid email"
                }
            }
        },
        hash: {
            allowNull: false,
            type: DataTypes.STRING(1024),
        },
        salt: {
            allowNull: false,
            type: DataTypes.STRING(32),
        }
    });

    return User;
};