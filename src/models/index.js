
require("dotenv").config();
const { DataTypes } = require("sequelize");
const Sequilize = require("sequelize");

const sequelize = new Sequilize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: "postgres",
    dialectOptions: {
        ssl: false,
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

let db = {};

db.DataTypes = DataTypes;
db.sequelize = sequelize;
db.Sequelize = Sequilize;

db.tag = require("../models/Tag")(sequelize, DataTypes);
db.user = require("../models/User")(sequelize, DataTypes);
db.file = require("../models/File")(sequelize, DataTypes);
db.group = require("../models/Group")(sequelize, DataTypes);
db.userGroup = require("../models/UserGroup")(sequelize, DataTypes);
db.filesTag = require("../models/FileTag")(sequelize, DataTypes);

db.permission = require("../models/Permission")(sequelize, DataTypes);


// Relations

db.user.belongsToMany(db.group, { through: db.userGroup });
db.group.belongsToMany(db.user, { through: db.userGroup });

db.file.belongsToMany(db.tag, { through: db.filesTag });
db.tag.belongsToMany(db.file, { through: db.filesTag });

db.file.hasMany(db.permission, { foreignKey: "fileId" });
db.permission.belongsTo(db.file);

db.file.hasMany(db.file, { foreignKey: "parent", as: "child", onDelete: "cascade" });
db.file.belongsTo(db.file, { foreignKey: "parent", as: "folder" });

db.file.belongsToMany(db.tag, { through: 'file_tags' });
db.tag.belongsToMany(db.file, { through: 'file_tags' });

db.user.hasMany(db.permission, { foreignKey: "userId" });
db.permission.belongsTo(db.user);

db.group.hasMany(db.permission, { foreignKey: "groupId" });
db.permission.belongsTo(db.group);


module.exports = db;