const router = require('express').Router();
const upload = require('../../utils/multer');
const auth = require('../auth');
const file = require("../../controllers/file.controller");
const permission = require("../../middlewares/permission.middleware");

// File routes
router.post("/upload", auth.required, upload.array('files'), file.upload);
router.get("/download/:id", auth.required, permission.check("read"), file.download);
router.put("/:id", auth.required, permission.check("update"), file.update);
router.delete("/:id", auth.required, permission.check("delete"), file.delete);

router.post("/folder/create", auth.required, file.createFolder);
router.delete("/folder/:id", auth.required, permission.check("delete"), file.deleteFolder);
router.get("/folder/detail/:id", auth.required, permission.check("read"), file.folderDetails);

module.exports = router;