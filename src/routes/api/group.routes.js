const router = require('express').Router();
const auth = require('../auth');
const group = require("../../controllers/group.controller");

// Group routes
router.post("/create", auth.required, group.create);
router.put("/:id", auth.required, group.update);
router.delete("/:id", auth.required, group.delete);
router.post("/list", auth.required, group.list);
router.get("/details/:id", auth.required, group.details);
router.post("/add/user", auth.required, group.addUser);


module.exports = router;