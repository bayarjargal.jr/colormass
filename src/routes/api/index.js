var router = require('express').Router();

router.use('/user', require('./user.routes'));
router.use('/file', require('./file.routes'));
router.use('/tag', require('./tag.routes'));
router.use('/group', require('./group.routes'));
router.use('/permission', require('./permission.routes'));
router.use(require("../../middlewares/error-handler"));

module.exports = router;
