const router = require('express').Router();
const upload = require('../../utils/multer');
const auth = require('../auth');
const permission = require("../../controllers/permission.controller");

// Folder routes
router.post("/grant", auth.required, permission.grant);


module.exports = router;