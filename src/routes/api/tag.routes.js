const router = require('express').Router();
const upload = require('../../utils/multer');
const auth = require('../auth');
const tag = require("../../controllers/tag.controller");
const permission = require("../../middlewares/permission.middleware");

// Folder routes
router.post("/file/add", auth.required, tag.addTag);

router.post("/create", auth.required, tag.create);
router.put("/:id", auth.required, tag.update);
router.delete("/:id", auth.required, tag.delete);
router.post("/list", auth.required, tag.list);
router.get("/details/:id", auth.required, tag.details);


module.exports = router;