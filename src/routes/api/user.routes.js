const controller = require("../../controllers/user.controller");
const router = require("express").Router();

router.post("/", controller.create);
router.delete("/:id", controller.delete);
router.post("/list", controller.list);
router.post("/login", controller.login);
router.put("/:id", controller.update);
router.use(require("../../middlewares/error-handler"));


module.exports = router;