const { expressjwt: jwt } = require("express-jwt");

function getTokenFromHeader(request) {
    if ((request.headers.authorization && request.headers.authorization.split(" ")[0] === "Token") ||
        (request.headers.authorization && request.headers.authorization.split(" ")[0] === "Bearer")
    )
        return request.headers.authorization.split(" ")[1];
    return null;
}


const auth = {
    required: jwt({
        secret: process.env.JWT_SECRET,
        userProperty: "payload",
        getToken: getTokenFromHeader,
        algorithms: ['HS256'],
    }),
    optional: jwt({
        secret: process.env.JWT_SECRET,
        userProperty: "payload",
        getToken: getTokenFromHeader,
        credentialsRequired: false,
        algorithms: ['HS256']
    })
}
module.exports = auth;