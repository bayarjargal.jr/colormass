var router = require('express').Router();

router.use('/api', require('./api'));
router.use(require("../middlewares/error-handler"));


module.exports = router;