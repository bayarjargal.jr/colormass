const aws = require('aws-sdk')
aws.config.update({
    secretAccessKey: process.env.AWS_ACL_KEY,
    accessKeyId: process.env.AWS_ACL_ID,
    region: 'eu-central-1'
});

module.exports = aws;