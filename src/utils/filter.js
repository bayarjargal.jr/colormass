const { Op } = require("sequelize");

exports.filter = async (model, query, _pagination = { size: 10, page: 0 }) => {

    console.log(JSON.stringify(query))

    let options = { where: {} }
    query.forEach(element => {
        if (element.searchType === "like") {
            options.where[element.fieldName] = {
                [Op.like]: `%${element.fieldValue}%`
            }

        } else if (element.searchType === "equal")
            options.where[element.fieldName] = element.fieldValue;
        else
            options.where[element.fieldName] = {
                [Op[element.searchType]]: element.fieldValue
            };

    });

    try {

        const { rows, count } = await model.findAndCountAll({ ...options, limit: _pagination.size, offset: _pagination.page * _pagination.size });;
        return { rows, count };

    } catch (error) {
        return { rows: [], count: 0 };
    }

}
