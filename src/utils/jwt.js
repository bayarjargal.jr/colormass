
require("dotenv").config();
const jwt = require('jsonwebtoken');

const getTokenFromHeader = request => request.headers.authorization.split(" ")[1];

exports.generateJWT = function (id) {
    var today = new Date();
    var exp = new Date(today);
    exp.setDate(exp.getDate() + 1);

    return jwt.sign({
        id: id,
        username: id,
        exp: parseInt(exp.getTime() / 1000),
    }, process.env.JWT_SECRET);
};

exports.getUserIDFromJWT = function (request) {
    const token = getTokenFromHeader(request);
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    return decoded.id;
}