const path = require('path')

const multer = require("multer");
const aws = require('./aws')
const multerS3 = require('multer-s3');
const { format } = require('date-fns');
require("dotenv").config();


const s3 = new aws.S3();

const storage = multerS3({
    s3: s3,
    acl: 'public-read',
    bucket: 'colormass',
    key: function (req, file, cb) {
        console.log(file);
        cb(null, `${path.parse(file.originalname).name}_${format(new Date(), "yyyy_MM_dd_HH_mm_ss")}${path.extname(file.originalname)}`); //use Date.now() for unique file keys
    }
});

const diskStorage = multer.diskStorage({
    destination: (req, res, callback) => {
        callback(null, path.join("uploads"))
    },
    filename: (req, file, callback) => {
        callback(null, `${path.parse(file.originalname).name}_${format(new Date(), "yyyy_MM_dd_HH_mm_ss")}${path.extname(file.originalname)}`)
    }

})

/**
 * Upload to AWS S3 bucket if ACL key and ID provided, otherwise upload to disk.
 */

if (process.env.AWS_ACL_KEY && process.env.AWS_ACL_ID) console.log("Using S3 bucket as a storage");
else console.log("Using disk as a storage")
const upload = multer({ storage: process.env.AWS_ACL_KEY && process.env.AWS_ACL_ID ? storage : diskStorage });

module.exports = upload;