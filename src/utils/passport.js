const passport = require("passport");
const crypto = require("crypto");
const LocalStrategy = require("passport-local");

const db = require("../models");
const User = db.user;

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, function (email, password, done) {
    User.findOne({ where: { email } }).then(function (user) {
        console.log(user)
        if (!user)
            return done(null, false, { name: "Access denied", errors: ["E-Mail is not registered"] })
        var hash = crypto.pbkdf2Sync(password, user.dataValues.salt, 10000, 512, 'sha512').toString('hex');
        if (user.dataValues.hash !== hash) {
            return done(null, false, { name: "Access denied", errors: ["Invalid password"] });
        }

        return done(null, user);
    }).catch(done);
}));