const yup = require("yup");

module.exports = yup.object({
    name: yup.string().required("Group name is required"),
})