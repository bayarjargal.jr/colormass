const yup = require("yup");

module.exports = yup.object({
    firstname: yup.string().required("First name is required"),
    lastname: yup.string().required("Last name is required"),
    password: yup.string().required("Password is required").min(8, "Minimum length is 8"),
    email: yup.string().required("E-Mail is required").email("Invalid email"),
})